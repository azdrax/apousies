<?php
namespace app\console\controllers;

use yii\helpers\Console;
use yii\console\Controller;
use Yii;

/**
 * Creates base rbac authorization data for our application.
 * -----------------------------------------------------------------------------
 * Creates 6 roles:
 *
 * - theCreator : you, developer of this site (super admin)
 * - admin      : your direct clients, administrators of this site
 * - editor     : editor of this site
 * - support    : support staff
 * - premium    : premium member of this site
 * - member     : user of this site who has registered his account and can log in
 *
 * - guardian   : A student's parent or a legal guardian
 * - instructor : Teacher responsible for a section's (tmima) absences
 *
 *
 * Creates 7 permissions:
 *
 * - usePremiumContent  : allows premium members to use premium content
 * - createArticle      : allows editor+ roles to create articles
 * - updateOwnArticle   : allows editor+ roles to update own articles
 * - updateArticle      : allows admin+ roles to update all articles
 * - deleteArticle      : allows admin+ roles to delete articles
 * - adminArticle       : allows admin+ roles to manage articles
 * - manageUsers        : allows admin+ roles to manage users (CRUD plus role assignment)
 *
 * Creates 1 rule:
 *
 * - AuthorRule : allows editor+ roles to update their own content
 */
class RbacController extends Controller
{
    /**
     * Initializes the RBAC authorization data.
     */
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        //---------- RULES ----------//

        // add the rule
        $rule = new \app\rbac\rules\AuthorRule;
        $auth->add($rule);

        //---------- PERMISSIONS ----------//

        // add "usePremiumContent" permission
       /* $usePremiumContent = $auth->createPermission('usePremiumContent');
        $usePremiumContent->description = 'Allows premium+ roles to use premium content';
        $auth->add($usePremiumContent);*/

        // add "manageUsers" permission
        $manageUsers = $auth->createPermission('manageUsers');
        $manageUsers->description = 'Allows admin+ roles to manage users';
        $auth->add($manageUsers);

        // add "createArticle" permission
       /* $createArticle = $auth->createPermission('createArticle');
        $createArticle->description = 'Allows editor+ roles to create articles';
        $auth->add($createArticle);

        // add "deleteArticle" permission
        $deleteArticle = $auth->createPermission('deleteArticle');
        $deleteArticle->description = 'Allows admin+ roles to delete articles';
        $auth->add($deleteArticle);

        // add "adminArticle" permission
        $adminArticle = $auth->createPermission('adminArticle');
        $adminArticle->description = 'Allows admin+ roles to manage articles';
        $auth->add($adminArticle);  */

       /* // add "updateArticle" permission
        $updateArticle = $auth->createPermission('updateArticle');
        $updateArticle->description = 'Allows editor+ roles to update articles';
        $auth->add($updateArticle);*/

        // add the "updateOwnArticle" permission and associate the rule with it.
        /*$updateOwnArticle = $auth->createPermission('updateOwnArticle');
        $updateOwnArticle->description = 'Update own article';
        $updateOwnArticle->ruleName = $rule->name;
        $auth->add($updateOwnArticle);

        // "updateOwnArticle" will be used from "updateArticle"
        $auth->addChild($updateOwnArticle, $updateArticle);*/

        $createAbsence = $auth->createPermission('createAbsence');
        $createAbsence->description = 'Create a student\'s absence';
        $auth->add($createAbsence);

        $updateAbsence = $auth->createPermission('updateAbsence');
        $updateAbsence->description = 'Update a student\'s absence';
        $auth->add($updateAbsence);

        $deleteAbsence = $auth->createPermission('deleteAbsence');
        $deleteAbsence->description = 'Delete a student\'s absence';
        $auth->add($deleteAbsence);

        $createGuardian = $auth->createPermission('createGuardian');
        $createGuardian->description = 'Create a new guardian';
        $auth->add($createGuardian);

        $updateGuardian = $auth->createPermission('updateGuardian');
        $updateGuardian->description = 'Update guardian';
        $auth->add($updateGuardian);

        $deleteGuardian = $auth->createPermission('deleteGuardian');
        $deleteGuardian->description = 'Delete guardian';
        $auth->add($deleteGuardian);

        $createSection = $auth->createPermission('createSection');
        $createSection->description = 'Create a new section';
        $auth->add($createSection);

        $updateSection = $auth->createPermission('updateSection');
        $updateSection->description = 'Update section';
        $auth->add($updateSection);

        $deleteSection = $auth->createPermission('deleteSection');
        $deleteSection->description = 'Delete section';
        $auth->add($deleteSection);

        $createSchool = $auth->createPermission('createSchool');
        $createSchool->description = 'Create a new school';
        $auth->add($createSchool);

        $updateSchool = $auth->createPermission('updateSchool');
        $updateSchool->description = 'Update school';
        $auth->add($updateSchool);

        $deleteSchool = $auth->createPermission('deleteSchool');
        $deleteSchool->description = 'Delete school';
        $auth->add($deleteSchool);

        $createStudent = $auth->createPermission('createStudent');
        $createStudent->description = 'Create a new student';
        $auth->add($createStudent);

        $updateStudent = $auth->createPermission('updateStudent');
        $updateStudent->description = 'Update student';
        $auth->add($updateStudent);

        $deleteStudent = $auth->createPermission('deleteStudent');
        $deleteStudent->description = 'Delete student';
        $auth->add($deleteStudent);

        $createInstructor = $auth->createPermission('createInstructor');
        $createInstructor->description = 'Create a new instructor';
        $auth->add($createInstructor);

        $updateInstructor = $auth->createPermission('updateInstructor');
        $updateInstructor->description = 'Update instructor';
        $auth->add($updateInstructor);

        $deleteInstructor = $auth->createPermission('deleteInstructor');
        $deleteInstructor->description = 'Delete instructor';
        $auth->add($deleteInstructor);
        //---------- ROLES ----------//

        // add "member" role
        $member = $auth->createRole('member');
        $member->description = 'Registered users, members of this site';
        $auth->add($member);

        $guardian = $auth->createRole('guardian');
        $guardian->description = 'Student\'s parent or legal guardian';
        $auth->add($guardian);

        // add "premium" role
       /* $premium = $auth->createRole('premium');
        $premium->description = 'Premium members. They have more permissions than normal members';
        $auth->add($premium);
        $auth->addChild($premium, $usePremiumContent);*/

        // add "support" role
        // support can do everything that member and premium can, plus you can add him more powers
       /* $support = $auth->createRole('support');
        $support->description = 'Support staff';
        $auth->add($support); 
       // $auth->addChild($support, $premium);
        $auth->addChild($support, $member);    */

        // add "editor" role and give this role: 
        // createArticle, updateOwnArticle and adminArticle permissions, plus he can do everything that support role can do.
        $editor = $auth->createRole('editor');
        $editor->description = 'Editor of this application';
        $auth->add($editor);
       // $auth->addChild($editor, $support);
        $auth->addChild($editor, $createAbsence);
        $auth->addChild($editor, $deleteAbsence);
        $auth->addChild($editor, $updateAbsence);
        $auth->addChild($editor, $createGuardian);
        $auth->addChild($editor, $deleteGuardian);
        $auth->addChild($editor, $updateGuardian);
        $auth->addChild($editor, $createStudent);
        $auth->addChild($editor, $deleteStudent);
        $auth->addChild($editor, $updateStudent);
        $auth->addChild($editor, $createSection);
        $auth->addChild($editor, $deleteSection);
        $auth->addChild($editor, $updateSection);
        $auth->addChild($editor, $createSchool);
        $auth->addChild($editor, $deleteSchool);
        $auth->addChild($editor, $updateSchool);
       /* $auth->addChild($editor, $updateOwnArticle);
        $auth->addChild($editor, $adminArticle);*/

        // add "admin" role and give this role: 
        // manageUsers, updateArticle adn deleteArticle permissions, plus he can do everything that editor role can do.
        $admin = $auth->createRole('admin');
        $admin->description = 'Administrator of this application';
        $auth->add($admin);

        $auth->addChild($admin, $editor);
        $auth->addChild($admin, $createInstructor);
        $auth->addChild($admin, $updateInstructor);
        $auth->addChild($admin, $deleteInstructor);
        $auth->addChild($admin, $manageUsers);
   /*     $auth->addChild($admin, $updateArticle);
        $auth->addChild($admin, $deleteArticle);*/

        // add "theCreator" role ( this is you :) )
        // You can do everything that admin can do plus more (if You decide so)
        $theCreator = $auth->createRole('theCreator');
        $theCreator->description = 'You!';
        $auth->add($theCreator); 
        $auth->addChild($theCreator, $admin);

        if ($auth) 
        {
            $this->stdout("\nRbac authorization data are installed successfully.\n", Console::FG_GREEN);
        }
    }
}