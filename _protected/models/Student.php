<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student".
 *
 * @property integer $id
 * @property string $fname
 * @property string $lname
 * @property string $telephone
 * @property string $comments
 * @property integer $tmima_id
 *
 * @property Apousia[] $apousias
 * @property Section $tmima
 * @property StudentHasParent[] $studentHasParents
 * @property Guardian[] $kidemonas
 */
class Student extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tmima_id','fname','lname'], 'required'],
            [['tmima_id'], 'integer'],
            [['fname', 'lname'], 'string', 'max' => 50],
            [['telephone'], 'string', 'max' => 10],
            [['comments'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fname' => Yii::t('app', 'Fname'),
            'lname' => Yii::t('app', 'Lname'),
            'telephone' => Yii::t('app', 'Telephone'),
            'comments' => Yii::t('app', 'Comments'),
            'tmima_id' => Yii::t('app', 'Tmima ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApousias()
    {
        return $this->hasMany(Apousia::className(), ['mathitis_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTmima()
    {
        return $this->hasOne(Section::className(), ['id' => 'tmima_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentHasParents()
    {
        return $this->hasMany(StudentHasParent::className(), ['mathitis_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKidemonas()
    {
        return $this->hasMany(Guardian::className(), ['id' => 'kidemonas_id'])->viaTable('student_has_parent', ['mathitis_id' => 'id']);
    }
}
