<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "guardian".
 *
 * @property integer $id
 * @property string $lname
 * @property string $fname
 * @property string $telephone
 * @property string $email
 * @property string $cellphone
 * @property string $adress
 *
 * @property StudentHasParent[] $studentHasParents
 * @property Student[] $mathitis
 */
class Guardian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'guardian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lname', 'fname', 'email', 'adress'], 'string', 'max' => 45],
            [['telephone', 'cellphone'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lname' => Yii::t('app', 'Lname'),
            'fname' => Yii::t('app', 'Fname'),
            'telephone' => Yii::t('app', 'Telephone'),
            'email' => Yii::t('app', 'Email'),
            'cellphone' => Yii::t('app', 'Cellphone'),
            'adress' => Yii::t('app', 'Adress'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentHasParents()
    {
        return $this->hasMany(StudentHasParent::className(), ['kidemonas_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMathitis()
    {
        return $this->hasMany(Student::className(), ['id' => 'mathitis_id'])->viaTable('student_has_parent', ['kidemonas_id' => 'id']);
    }
}
