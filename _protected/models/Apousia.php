<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apousia".
 *
 * @property integer $id
 * @property string $date
 * @property integer $mathitis_id
 * @property integer $ora
 * @property integer $dikeologimeni
 *
 * @property Student $mathitis
 */
class Apousia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'apousia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['mathitis_id'], 'required'],
            [['mathitis_id', 'ora', 'dikeologimeni'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Date'),
            'mathitis_id' => Yii::t('app', 'Mathitis ID'),
            'ora' => Yii::t('app', 'Ora'),
            'dikeologimeni' => Yii::t('app', 'Dikeologimeni'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMathitis()
    {
        return $this->hasOne(Student::className(), ['id' => 'mathitis_id']);
    }
}
