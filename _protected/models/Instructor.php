<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "instructor".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ClassHasTeacher[] $classHasTeachers
 * @property Class[] $tmimas
 */
class Instructor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'instructor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassHasTeachers()
    {
        return $this->hasMany(ClassHasTeacher::className(), ['kathigitis_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTmimas()
    {
        return $this->hasMany(Class::className(), ['id' => 'tmima_id'])->viaTable('class_has_teacher', ['kathigitis_id' => 'id']);
    }
}
