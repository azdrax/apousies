<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "section".
 *
 * @property integer $id
 * @property string $name
 * @property string $class
 *
 * @property SectionHasTeacher[] $sectionHasTeachers
 * @property Instructor[] $kathigitis
 * @property Student[] $students
 */
class Section extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'section';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class'], 'string'],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'class' => Yii::t('app', 'Class'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectionHasTeachers()
    {
        return $this->hasMany(SectionHasTeacher::className(), ['tmima_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKathigitis()
    {
        return $this->hasMany(Instructor::className(), ['id' => 'kathigitis_id'])->viaTable('section_has_teacher', ['tmima_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['tmima_id' => 'id']);
    }
}
