<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Apousia;

/**
 * ApousiaSearch represents the model behind the search form about `app\models\Apousia`.
 */
class ApousiaSearch extends Apousia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mathitis_id', 'ora', 'dikeologimeni'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Apousia::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'mathitis_id' => $this->mathitis_id,
            'ora' => $this->ora,
            'dikeologimeni' => $this->dikeologimeni,
        ]);

        return $dataProvider;
    }
}
