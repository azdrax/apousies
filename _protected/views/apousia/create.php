<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Apousia */

$this->title = Yii::t('app', 'Create Apousia');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Apousias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="apousia-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
