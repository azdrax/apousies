<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Students');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Student'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText'=>'-',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          //  'id',
            'fname',
            'lname',
            'telephone',
            [
                'attribute'=>'tmima_id',
                'label'=>Yii::t('app', 'Τμήμα'),
                'value'=>'tmima.name',
            ],
            'comments',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
