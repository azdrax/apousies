<?php

return [

    '{attribute} cannot be blank.' => 'το {attribute} πρέπει να συμπληρωθεί.',
    'The verification code is incorrect.' => 'Ο κωδικός είναι λάθος.',
    'Home' => 'Αρχική',
    'You are not allowed to perform this action.' => 'Δεν έχετε τα δικαιώματα για αυτήν την ενέργεια.'
];