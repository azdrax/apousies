<?php

return [

    //-- general buttons --//
    'Submit' => 'Αποστολή',
    'Cancel' => 'Ακύρωση',
    'Update' => 'Ενημέρωση',
    'Delete' => 'Διαγραφή',
    'Create' => 'Δημιουργία',
    'Send'   => 'Αποστολή',
    'Save'   => 'Αποθήκευση',
    'Menu'   => 'Μενού',
    'Back'   => 'Πίσω',

    //-- CMS buttons --//
    'Create User' => 'Δημιουργία Χρήστη',
    'Create Article' => 'Δημιουργία άρθρου',

    //-- top menu --//
    'Home'     => 'Αρχική',
    'About'    => 'Περί',
    'Contact'  => 'Επικοινωνία',
    'Login'    => 'Είσοδος',
    'Logout'   => 'Έξοδος',
    'Users'    => 'Χρήστες',
    'Articles' => 'Άρθρα',
    'Register' => 'Εγγραφή',

    //-- static pages --//
    'Password' => 'Κωδικός',
    'Username' => 'Όνομα Χρήστη',
    
    // contact
    'Name'    => 'Όνομα',
    'Subject' => 'Θέμα',
    'Text'    => 'Κείμενο',
    'Verification Code' => 'Κωδικός',
    'If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.' 
        => 'Αν έχετε ερωτήσεις για τη σελίδα μας, παρακαλώ συμπληρώστε την παρακάτω φόρμα. Ευχαριστούμε',
    'Thank you for contacting us. We will respond to you as soon as possible.' => 'Ευχαριστούμε για την επικοινωνία. Θα σας απαντήσουμε το συντομότερο δυνατό.',
    'There was an error sending email.' => 'Παρουσιάστηκε κάποιο πρόβλημα κατά την αποστολή του μηνύματός σας.',

    // password reset
    'If you forgot your password you can' => 'Αν ξεχάσατε τον κωδικό σας πατείστε για ',
    'reset it' => 'επαναφορά',
    'Request password reset' => 'Αίτηση επαναφορά κωδικού πρόσβασης',
    'Please fill out your email.' => 'Παρακαλώ συμπληρώστε το email σας.',
    'Reset password' => 'Επαναφορά κωδικού πρόσβασης',
    'A link to reset password will be sent to your email.' => 'Θα αποσταλεί ένας σύνδεσμος επαναφοράς του κωδικού σας στο email σας.',
    'Check your email for further instructions.' => 'Για περισσότερες οδηγίες ελέξτε το email σας.',
    'Please choose your new password:' => 'Παρακαλώ, επιλέξτε ένα νέο κωδικός πρόσβασης:',
    'New password was saved.' => 'Ο νέος κωδικός αποθηκεύτηκε.',
    'Sorry, we are unable to reset password for email provided.' => 'Συγνώμη, δεν μπορέσαμε να επαναφέρουμε τον κωδικό για τη συγκεκριμένη διεύθυνση email.',
    'Password reset token cannot be blank.' => 'Token ne može biti prazan.',
    'Wrong password reset token.' => 'Pogrešan token.',

    // signup
    'Signup'   => 'Εγγραφή',
    'Please fill out the following fields to signup:' => 'Συπληρώστε την παρακάτω φόρμα για να εγγραφείτε:',
    'We will send you an email with account activation link.' => 'Θα σας αποστελεί email για την ενεργοποίηση του λογαριασμού σας.',
    'We couldn\'t sign you up, please contact us.' => 'Η εγγραφή σας δεν ολοκληρώθηκε. Παρακαλούμε επικοινωνείστε μαζί μας.',

    // login
    'Remember me'  => 'Να με θυμάσαι',
    'Please fill out the following fields to login:' => 'Συμπληρώστε τη φόρμα για να συνδεθείτε:',
    'You have to activate your account first. Please check your email.' => 'Πρέπει πρώτα να ενεργοποιήσετε το λογαριασμό σας. Παρακαλώ ελέξτε το email σας..',
    'To be able to log in, you need to confirm your registration. Please check your email, we have sent you a message.'
        => 'Για να μπορέσετε να εγγραφείτε πρέπει να επιβεβαιώσετε την εγγραφή σας. Σας έχουμε στείλει ένα μήνυμα, παρακαλούμε, ελέξτε το email σας.',

    // account activation
    'We couldn\'t send you account activation email, please contact us.' => 'Nismo mogli da Vam pošaljemo email sa linkom za aktivaciju naloga. Molimo Vas da nas kontaktirate.',
    'Success! You can now log in.' => 'Ωραία! Τωρα μπορείτε να συνδεθείτε.',
    'for joining us!' => 'što ste nam se pridružili.',
    'your account could not be activated, please contact us!' => 'ο λογαριασμός σας δεν μπορεί να ενεργοποιηθεί, παρακαλώ επινοινωνήστε μαζί μας.',
    'Account activation token cannot be blank.' => 'Token ne može biti prazan.',
    'Wrong account activation token.' => 'Pogrešan token.',

    //-- general db fields --//
    'Created At' => 'Δημιουργήθηκε στις',
    'Updated At' => 'Ενημερώθηκε στις',

    //-- mixed --//
    'My Company' => 'Η Εταιρεία μου',
    'Hello' => 'Γεια σου',
    'Thank you' => 'Ευχαριστώ',
    
    //-- users management --//
    'Role' => 'Ρόλος',
    'Create User' => 'Δημιουργία Χρήστη',
    'Update User' => 'Ενημέρωση Χρήστη',
    'New pwd ( if you want to change it )' => 'Νέος κωδικός (αν θέλετε να τον αλλάξετε)',

    //-- articles --//
    'Title'    => 'Naslov',
    'Summary'  => 'Rezime',
    'Content'  => 'Sadržaj',
    'Status'   => 'Status',
    'Category' => 'Kategorija',
    'Author'   => 'Autor',
    'articles' => 'članci',
    'news'     => 'vesti',
    'The best news available' => 'Najbolje vesti na tržištu',
    'We haven\'t created any articles yet.' => 'Još uvek nismo napisali nijedan članak.',
    'Read more'    => 'Pročitaj sve',
    'Published on' => 'Objavljen',

    // statuses
    'Draft'     => 'Nacrt',
    'Published' => 'Objavljen',
    'Archived'  => 'Arhiviran',

    // categories
    'Economy' => 'Ekonomija',
    'Society' => 'Društvo',
    'Sport'   => 'Sport',

    //-- errors --//
    'The above error occurred while the Web server was processing your request.' => 'Dogodila se gore pomenuta greška kada je server pokušao da izvrši Vaš zahtev.',
    'Please contact us if you think this is a server error. Thank you.' => 'Molimo Vas kontaktirajte nas ukoliko mislite da je ovo greška našeg servera. Hvala.',
    'You are not allowed to access this page.' => 'Nije Vam dozvoljeno da vidite ovu stranicu',

    //-- delete messages --//
    'Are you sure you want to delete this user?' => 'Σίγουρα θέλεις να διαγράψεις το χρήστη αυτό;',
    'Are you sure you want to delete this article?' => 'Da li ste sigurni da želite da obrišete ovu vest?',

    'Student'=>'Μαθητής',
    'Students'=>'Μαθητές',
    'Section'=>'Τμήμα',
    'Sections'=>'Τμήματα',
    'School'=>'Σχολείο',
    'Schools'=>'Σχολεία',

    /* Καταστάσεις χρηστών */
    'Active'=>'Ενεργός',
    'Inactive'=>'Ανενεργός',
    'Deleted'=>'Διεγραμμένος',
    'Status'=>'Κατάσταση',

    /* Μαθητές*/
    'ID'=>'Α/Α',
    'Fname'=>'Όνομα',
    'Lname'=>'Επίθετο',
    'Telephone'=>'Τηλέφωνο',
    'Comments'=>'Σχόλια',
    'Tmima ID'=>'Τμήμα',
    'Create Student'=>'Νέος Μαθητής',


    /* Μηνύματα action column  */

    'View'=>'Προβολή',
   // 'Update'=>'Ενημέρωση',
   // 'Delete'=>'Διαγραφή',
];