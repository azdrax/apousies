Apousies
========
A web application for allowing greece's secondary school:

* Staff inform inform guardians
* Students' parents view their children's

school absences.
It is based on [Yii2 framework](http://www.yiiframework.com/).
It uses [Nenad Zivkovic's advanced template for Yii2](https://github.com/nenad-zivkovic/yii2-advanced-template).